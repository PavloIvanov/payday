//
//  UserService.swift
//  PayDayNetworking
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

public protocol UserNetworkServiceProtocol: class {
    func login<T: Decodable>(email: String,
                             password: String,
                             responseHandler: ResponseHandlerProtocol?,
                             completion: @escaping (Result<T>) -> Void)
    func registerUser<T: Encodable>(with model: T,
                                    email: String,
                                    phone: String,
                                    responseHandler: ResponseHandlerProtocol?,
                                    completion: @escaping (Result<Int>) -> Void)
}

public class UserNetworkService: UserNetworkServiceProtocol {

    private let serviceLocator: ServiceLocatorProtocol

    public init(serviceLocator: ServiceLocatorProtocol? = nil) {
        self.serviceLocator = serviceLocator ?? ServiceLocator.sharedInstance
    }

    public func login<T>(email: String,
                         password: String,
                         responseHandler: ResponseHandlerProtocol?,
                         completion: @escaping (Result<T>) -> Void) where T: Decodable {
        login(email: email,
              password: password,
              responseHandler: responseHandler ?? ResponseHandler(),
              completion: completion)
    }

    private func login<T>(email: String,
                          password: String,
                          responseHandler: ResponseHandlerProtocol,
                          completion: @escaping (Result<T>) -> Void) where T: Decodable {
        let parameters = ["email": email,
                          "password": password]
        serviceLocator.getRouter(Router<UserEndPoint>.self)?.request(.login(parameters: parameters), completion: { (data, response, error) in
            let result = responseHandler.handleResponse(T.self, data, response, error)
            completion(result)
        })
    }

    public func registerUser<T>(with model: T,
                                email: String,
                                phone: String,
                                responseHandler: ResponseHandlerProtocol?,
                                completion: @escaping (Result<Int>) -> Void) where T: Encodable {
        registerUser(with: model,
                     email: email,
                     phone: phone,
                     responseHandler: responseHandler ?? ResponseHandler(),
                     completion: completion)
    }

    private func registerUser<T>(with model: T,
                                 email: String,
                                 phone: String,
                                 responseHandler: ResponseHandlerProtocol,
                                 completion: @escaping (Result<Int>) -> Void) where T: Encodable {
        var isEmailExistingFlag = false
        var isPhoneExistingFlag = false

        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        checkEmailExisting(email: email) { (isEmailExisting) in
            isEmailExistingFlag = isEmailExisting
            dispatchGroup.leave()
        }

        dispatchGroup.enter()
        checkPhoneExisting(phone: phone) { (isPhoneExisting) in
            isPhoneExistingFlag = isPhoneExisting
            dispatchGroup.leave()
        }

        dispatchGroup.notify(queue: .main) {
            if !isEmailExistingFlag, !isPhoneExistingFlag {
                self.registerUser(with: model, responseHandler: responseHandler, completion: completion)
            } else {
                var message = isEmailExistingFlag ? "User with this email already exists. " : ""
                message += isPhoneExistingFlag ? "User with this phone already exists." : ""
                completion(.failure(message))
            }
        }
    }

    // MARK: - Helper Methods
    private func registerUser<T>(with model: T,
                                 responseHandler: ResponseHandlerProtocol,
                                 completion: @escaping (Result<Int>) -> Void)  where T: Encodable {

        guard let data = try? JSONEncoder().encode(model) else {
            completion(.failure("Something went wrong."))
            return
        }

        serviceLocator.getRouter(Router<UserEndPoint>.self)?.request(.register(data: data), completion: { (responseData, _, error) in

            if let errorMessage = error?.localizedDescription {
                completion(.failure(errorMessage))
                return
            }

            guard let unwrappedData = responseData,
                let result = try? JSONSerialization.jsonObject(with: unwrappedData, options: .mutableContainers) as? [String: AnyObject],
                let userId = result["id"] as? Int else {
                completion(.failure("Something went wrong."))
                return
            }

            completion(.success(userId))
        })
    }

    private func checkEmailExisting(email: String, completion: @escaping (_ isExisting: Bool) -> Void) {
        serviceLocator.getRouter(Router<CustomersEndPoint>.self)?.request(.checkEmail(email: email), completion: { (data, _, _) in
            if let data = data,
                let jsonData = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [AnyObject],
                !jsonData.isEmpty {
                completion(true)
                return
            }

            completion(false)
        })
    }

    private func checkPhoneExisting(phone: String, completion: @escaping (_ isExisting: Bool) -> Void) {
        serviceLocator.getRouter(Router<CustomersEndPoint>.self)?.request(.checkPhone(phone: phone), completion: { (data, _, _) in
            if let data = data,
                let jsonData = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [AnyObject],
                !jsonData.isEmpty {
                completion(true)
                return
            }

            completion(false)
        })
    }
}
