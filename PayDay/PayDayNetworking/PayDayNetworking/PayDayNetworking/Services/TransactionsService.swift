//
//  TransactionsService.swift
//  PayDayNetworking
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

public protocol TransactionsServiceProtocol: class {
    func getAllTransactions<T: Decodable>(accountId: Int,
                                          responseHandler: ResponseHandlerProtocol?,
                                          completion: @escaping (Result<[T]>) -> Void)
}

public class TransactionsService: TransactionsServiceProtocol {

    private let serviceLocator: ServiceLocatorProtocol

    public init(serviceLocator: ServiceLocatorProtocol? = nil) {
        self.serviceLocator = serviceLocator ?? ServiceLocator.sharedInstance
    }

    public func getAllTransactions<T>(accountId: Int,
                                      responseHandler: ResponseHandlerProtocol?,
                                      completion: @escaping (Result<[T]>) -> Void) where T: Decodable {
        getAllTransactions(accountId: accountId,
                           responseHandler: responseHandler ?? ResponseHandler(),
                           completion: completion)
    }

    private func getAllTransactions<T>(accountId: Int,
                                      responseHandler: ResponseHandlerProtocol,
                                      completion: @escaping (Result<[T]>) -> Void) where T: Decodable {
        serviceLocator.getRouter(Router<TransactionsEndPoint>.self)?.request(.getTransactions(accountId: accountId),
                                                                             completion: { (data, response, error) in
            let result = responseHandler.handleResponse([T].self, data, response, error)
            completion(result)
        })
    }
}
