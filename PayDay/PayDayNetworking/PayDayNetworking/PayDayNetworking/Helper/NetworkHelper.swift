//
//  NetworkHelper.swift
//  PINetworkLayer
//
//  Created by Pavlo Ivanov on 12/19/18.
//  Copyright © 2018 Pavlo Ivanov. All rights reserved.
//

import Foundation

public enum NetworkResponse: String {
    case success
    case authenticationError = "Login or password is incorrect."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

public enum Result<Value> {
    case success(Value)
    case failure(String)
}

public enum NetworkEnvironment {
    case production
}

public class NetworkHelper {

    private static let uniqueInstance = NetworkHelper()
    private static let environment: NetworkEnvironment = .production

    // MARK: - Properties
    static var environmentBaseURL: String {
        switch NetworkHelper.environment {
        case .production: return NetworkHelper.productionServerUrlString()
        }
    }

    class var sharedInstance: NetworkHelper {
        return uniqueInstance
    }

    // MARK: - Methods
    private init () {

    }

    private static func productionServerUrlString() -> String {
        if let path = Bundle.payDayNetworking.path(forResource: "Info", ofType: "plist") {
            let dictRoot = NSDictionary(contentsOfFile: path)
            if let dict = dictRoot {
                return dict["ProductionServerUrl"] as? String ?? ""
            }
        }

        return ""
    }
}
