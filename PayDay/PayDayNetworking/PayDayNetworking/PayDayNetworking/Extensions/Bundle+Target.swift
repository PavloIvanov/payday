//
//  Bundle+Target.swift
//  PayDayNetworking
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

private final class PayDayNetworkingBundleHelper { }

extension Bundle {
    static var payDayNetworking: Bundle {
        return Bundle(for: PayDayNetworkingBundleHelper.self)
    }
}
