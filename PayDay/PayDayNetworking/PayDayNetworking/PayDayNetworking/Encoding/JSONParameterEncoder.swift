//
//  JSONParameterEncoder.swift
//  PINetworkLayer
//
//  Created by Pavlo Ivanov on 12/19/18.
//  Copyright © 2018 Pavlo Ivanov. All rights reserved.
//

import Foundation

public struct JSONParameterEncoder: ParameterEncoder {
    public func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
        do {
            let jsonAsData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            urlRequest.httpBody = jsonAsData
            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                urlRequest.setValue("gzip, deflate, br", forHTTPHeaderField: "Accept-Encoding")
            }
        } catch {
            throw NetworkError.encodingFailed
        }
    }
}
