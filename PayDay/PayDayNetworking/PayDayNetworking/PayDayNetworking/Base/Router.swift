//
//  Router.swift
//  PINetworkLayer
//
//  Created by Pavlo Ivanov on 12/19/18.
//  Copyright © 2018 Pavlo Ivanov. All rights reserved.
//

import Foundation

public typealias Completion = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void

public protocol URLSessionProtocol: class {
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

public protocol NetworkRouterProtocol: class {
    associatedtype EndPoint: EndPointType
    func request(_ route: EndPoint,
                 urlSession: URLSessionProtocol?,
                 completion: @escaping Completion)
    func cancel()
}

public class Router<EndPoint: EndPointType>: NetworkRouterProtocol {
    private var task: URLSessionTask?

    public func request(_ route: EndPoint,
                        urlSession: URLSessionProtocol? = URLSession.shared,
                        completion: @escaping Completion) {
        do {
            let request = try route.buildRequest()
            #if DEBUG
            NetworkLogger.log(request: request)
            #endif
            task = urlSession?.dataTask(with: request, completionHandler: { data, response, error in
                completion(data, response, error)
            })
        } catch {
            completion(nil, nil, error)
        }
        self.task?.resume()
    }

    public func cancel() {
        self.task?.cancel()
    }
}

extension URLSession: URLSessionProtocol { }
