//
//  HTTPMethod.swift
//  PINetworkLayer
//
//  Created by Pavlo Ivanov on 12/19/18.
//  Copyright © 2018 Pavlo Ivanov. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}
