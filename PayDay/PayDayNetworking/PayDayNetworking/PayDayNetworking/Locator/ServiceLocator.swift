//
//  ServiceLocator.swift
//  NBTV-tvos
//
//  Created by Pavlo Ivanov on 12/19/18.
//  Copyright © 2018 appletv. All rights reserved.
//

import Foundation

public protocol ServiceLocatorProtocol: class {
    func getRouter<T>(_ routerProtocol: T.Type) -> T?
    func cancelAllRequests()
}

public class ServiceLocator: ServiceLocatorProtocol {

    private static var uniqueInstance = ServiceLocator()
    private var serviceCache = [String: Any]()

    public class var sharedInstance: ServiceLocator {
        return uniqueInstance
    }

    private init () {
        registerRouters()
    }

    private func registerRouters() {
        serviceCache["\(Router<UserEndPoint>.self)"] = Router<UserEndPoint>()
        serviceCache["\(Router<TransactionsEndPoint>.self)"] = Router<TransactionsEndPoint>()
        serviceCache["\(Router<CustomersEndPoint>.self)"] = Router<CustomersEndPoint>()
    }

    public func getRouter<T>(_ routerProtocol: T.Type) -> T? {
        return serviceCache["\(routerProtocol)"] as?  T
    }

    public func cancelAllRequests() {
        serviceCache.values.forEach { (_) in
            // TODO: (router as? NetworkRouterProtocol)?.cancel()
        }
    }
}
