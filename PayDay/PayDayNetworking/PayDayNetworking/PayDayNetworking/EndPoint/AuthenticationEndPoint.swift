//
//  AuthenticationEndPoint.swift
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

public enum UserEndPoint {
    case login(parameters: Parameters)
    case register(data: Data)
}

extension UserEndPoint: EndPointType {

    public var baseURL: URL {
        guard let url = URL(string: NetworkHelper.environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }

    public var path: String {
        switch self {
        case .login: return "authenticate"
        case .register: return "customers"
        }
    }

    public var httpMethod: HTTPMethod {
        return .post
    }

    public var task: HTTPTask {
        switch self {
        case .login(parameters: let params):
            return .requestParameters(bodyParameters: params,
                                      bodyEncoding: .jsonEncoding,
                                      urlParameters: nil)
        case .register(data: let data):
            return .requestData(data: data)
        }
    }

    public var headers: HTTPHeaders? {
        return nil
    }
}
