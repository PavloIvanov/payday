//
//  TransactionsEndPoint.swift
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

public enum TransactionsEndPoint {
    case getTransactions(accountId: Int)
}

extension TransactionsEndPoint: EndPointType {

    public var baseURL: URL {
        guard let url = URL(string: NetworkHelper.environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }

    public var path: String {
        switch self {
        case .getTransactions: return "transactions"
        }
    }

    public var httpMethod: HTTPMethod {
        return .get
    }

    public var task: HTTPTask {
        switch self {
        case .getTransactions(accountId: let accId):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["accountId": accId])
        }
    }

    public var headers: HTTPHeaders? {
        return nil
    }
}
