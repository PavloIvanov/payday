//
//  CustomersEndPoint.swift
//  PayDayNetworking
//
//  Created by Pavlo Ivanov on 31.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

public enum CustomersEndPoint {
    case checkEmail(email: String)
    case checkPhone(phone: String)
}

extension CustomersEndPoint: EndPointType {

    public var baseURL: URL {
        guard let url = URL(string: NetworkHelper.environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }

    public var path: String {
        return "customers"
    }

    public var httpMethod: HTTPMethod {
        return .get
    }

    public var task: HTTPTask {
        switch self {
        case .checkEmail(email: let email):
            let urlParams: [String: AnyObject] = ["email": email as AnyObject]

            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: urlParams)

        case .checkPhone(phone: let phone):
            let urlParams: [String: AnyObject] = ["phone": phone as AnyObject]

            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: urlParams)
        }
    }

    public var headers: HTTPHeaders? {
        return nil
    }
}
