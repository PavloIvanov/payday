//
//  RoutingService.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

protocol ApplicationRoutingDelegate: class {
    func changeRootViewController(with viewController: UIViewController)
}

protocol RoutingServiceProtocol: class {
    func changeRootViewController(with viewController: UIViewController)
}

class RoutingService: RoutingServiceProtocol {

    private weak var applicationRoutingDelegate: ApplicationRoutingDelegate?

    init(applicationRoutingDelegate: ApplicationRoutingDelegate?) {
        self.applicationRoutingDelegate = applicationRoutingDelegate
    }

    func changeRootViewController(with viewController: UIViewController) {
        applicationRoutingDelegate?.changeRootViewController(with: viewController)
    }
}
