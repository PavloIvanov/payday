//
//  ValidationService.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

enum ValidationResult {
    case success
    case error(message: String)
}

protocol ValidationServiceProtocol {
    func isValidPassword(_ password: String) -> ValidationResult
}

class ValidationService: ValidationServiceProtocol {
    func isValidPassword(_ password: String) -> ValidationResult {
        if password.count >= 6 {
            return .success
        }

        return .error(message: "Password is invalid")
    }
}
