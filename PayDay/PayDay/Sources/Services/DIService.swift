//
//  DIService.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation
import Swinject
import PayDayStorage
import PayDayNetworking

protocol DIContainerProtocol: class {
    func boot()
    func resolve<T>(_ service: T.Type) -> T?
}

class DIContainer: DIContainerProtocol {

    // MARK: - Public Properties
    static var shared: DIContainer {
        if let container = UIApplication.shared.diContainer as? DIContainer {
            return container
        } else {
            UIApplication.shared.diContainer = DIContainer()
            return DIContainer.shared
        }
    }

    // MARK: - Private Properties
    private let container: Container = Container()

    // MARK: - Public Methods
    func boot() {
        registerServices()
    }

    func resolve<T>(_ service: T.Type) -> T? {
        return container.resolve(service)
    }

    // MARK: - Private Methods
    private func registerServices() {
        container.register(UserServiceProtocol.self) { _ -> UserServiceProtocol in
            return UserService(userDefaults: UserDefaults.standard)
        }.inObjectScope(.container)

        container.register(TransactionsStorageProtocol.self) { _ -> TransactionsStorageProtocol in
            return TransactionsStorage()
        }.inObjectScope(.container)

        container.register(TransactionsServiceProtocol.self) { _ -> TransactionsServiceProtocol in
            return TransactionsService()
        }.inObjectScope(.container)

        container.register(UserNetworkServiceProtocol.self) { _ -> UserNetworkServiceProtocol in
            return UserNetworkService()
        }.inObjectScope(.container)

        container.register(ValidationServiceProtocol.self) { _ -> ValidationServiceProtocol in
            return ValidationService()
        }.inObjectScope(.container)

        container.register(AlertViewHelperProtocol.self) { _ -> AlertViewHelperProtocol in
            return AlertViewHelper()
        }.inObjectScope(.container)

        container.register(RoutingServiceProtocol.self) { _ -> RoutingServiceProtocol in
            return RoutingService(applicationRoutingDelegate: UIApplication.shared.delegate as? AppDelegate)
        }.inObjectScope(.container)
    }
}

extension UIApplication {

    struct AssociatedKeys {
        static var dependencyContainer: UInt8 = 0
    }

    var diContainer: DIContainerProtocol? {
        get {
            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.dependencyContainer) as? DIContainerProtocol else {
                return nil
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedKeys.dependencyContainer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
