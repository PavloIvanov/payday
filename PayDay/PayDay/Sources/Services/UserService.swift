//
//  UserService.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 29.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

// TODO: - Cover all needed methods of storage with protocol for future possibility to change it to another storage
protocol UserServiceProtocol {
    func saveUserInfo(_ user: UserModel)
    func set(value: Any, for key: String)
    func getValue(for key: String) -> Any?
    func clean()
}

struct UserServiceKeys {
    static let isLoggedInKey = "isLoggedIn"
    static let userId = "userId"
}

class UserService: UserServiceProtocol {

    private var shared: UserDefaults

    init(userDefaults: UserDefaults) {
        shared = UserDefaults.standard
    }

    func set(value: Any, for key: String) {
        shared.set(value, forKey: key)
    }

    func getValue(for key: String) -> Any? {
        return shared.value(forKey: key)
    }

    func saveUserInfo(_ user: UserModel) {
        shared.set(true, forKey: UserServiceKeys.isLoggedInKey)
        shared.set(user.identifier, forKey: UserServiceKeys.userId)
    }

    func clean() {
        shared.set(false, forKey: UserServiceKeys.isLoggedInKey)

        guard let domain = Bundle.main.bundleIdentifier else { return }
        shared.removePersistentDomain(forName: domain)
    }
}
