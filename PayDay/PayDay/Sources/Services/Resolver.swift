//
//  Resolver.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 03.08.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

var unitTestDependencyContainer: DIContainerProtocol?

func resolveService<T>(_ serviceType: T.Type) -> T? {
    if let testDIContainer = unitTestDependencyContainer {
        return unitTestDependencyContainer?.resolve(serviceType)
    } else {
        return UIApplication.shared.diContainer?.resolve(serviceType)
    }
}
