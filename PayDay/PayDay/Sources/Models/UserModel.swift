//
//  UserModel.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 31.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

struct UserModel: Codable {
    let identifier: Int
    let firstName: String
    let lastName: String
    let gender: String
    let email: String
    let password: String
    let dob: String
    let phone: String

    private enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case firstName = "First Name"
        case lastName = "Last Name"
        case gender
        case email
        case password
        case dob
        case phone
    }
}
