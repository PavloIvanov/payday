//
//  DailyGroupedTransactions.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

struct DailyGroupedTransactions: Hashable {
    var date: Date
    var transactions: [TransactionModel]
}
