//
//  TransactionModel.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

struct TransactionModel: Hashable, Decodable {
    // swiftlint:disable:next identifier_name
    var id: Int
    var accountId: Int
    var amount: String
    var vendor: String
    var category: String
    var date: String

    private enum CodingKeys: String, CodingKey {
        // swiftlint:disable:next identifier_name
        case id
        case accountId = "account_id"
        case amount
        case vendor
        case category
        case date
    }
}
