//
//  MonthlyExpensesModel.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

struct MonthlyExpensesModel: Decodable {
    var date: Date
    var sum: Double
    var expenses: [CategoryExpensesModel]
}

struct CategoryExpensesModel: Decodable {
    var categoryName: String
    var amount: Double
}
