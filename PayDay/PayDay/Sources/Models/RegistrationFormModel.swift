//
//  RegistrationFormModel.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

enum Gender: String, RawRepresentable, CaseIterable, Codable {
    case male = "Male"
    case female = "Female"
}

struct RegistrationFormModel: Encodable {
    var firstName: String
    var lastName: String
    var phoneNumber: String
    var email: String
    var password: String
    var confirmPassword: String?
    var gender: String
    var dateOfBirth: Date

    private enum CodingKeys: String, CodingKey {
        case firstName = "First Name"
        case lastName = "Last Name"
        case phoneNumber = "phone"
        case gender
        case email
        case password
        case dateOfBirth = "dob"
    }
}
