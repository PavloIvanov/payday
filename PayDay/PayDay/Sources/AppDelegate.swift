//
//  AppDelegate.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)

        DIContainer.shared.boot()
        setupPods()

        let isLoggedIn = resolveService(UserServiceProtocol.self)?.getValue(for: UserServiceKeys.isLoggedInKey) as? Bool
        setupRootViewController(isUserLoggedIn: isLoggedIn ?? false)

        return true
    }

    private func setupRootViewController(isUserLoggedIn: Bool) {
        var viewController: UIViewController = LoginViewController()

        if isUserLoggedIn {
            viewController = UINavigationController(rootViewController: TransactionsViewController())
        }

        changeRootViewController(with: viewController)
    }

    private func setupPods() {
        IQKeyboardManager.shared.enable = true
    }
}

extension AppDelegate: ApplicationRoutingDelegate {
    func changeRootViewController(with viewController: UIViewController) {
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
    }
}
