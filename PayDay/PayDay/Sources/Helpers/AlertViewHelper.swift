//
//  AlertViewHelper.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

protocol AlertViewHelperProtocol {
    func createOKAlertController(title: String, message: String) -> UIAlertController
}

class AlertViewHelper: AlertViewHelperProtocol {
    func createOKAlertController(title: String, message: String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            alertController.dismiss(animated: false, completion: nil)
        }

        alertController.addAction(okAction)

        return alertController
    }
}
