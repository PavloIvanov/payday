//
//  DateFormatterHelper.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

enum DateFormatType: String, RawRepresentable {
    case general = "yyyy-MM-dd'T'HH:mm:ssZ"
    case transaction = "MMM dd, yyyy"
    case dailyGroupedTransactions = "MMM dd"
    case dashboardMonthYear = "MMM yyyy"
}

class DateFormatterHelper {

    static func stringToDate(string: String, format: DateFormatType) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        return dateFormatter.date(from: string)
    }

    static func stringFromDate(date: Date, to format: DateFormatType) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        return dateFormatter.string(from: date)
    }

    static func getTransactionDateString(from string: String) -> String? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.locale = Locale(identifier: "en_US_POSIX")
        dateFormatterGet.dateFormat = DateFormatType.general.rawValue

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.locale = Locale(identifier: "en_US_POSIX")
        dateFormatterPrint.dateFormat = DateFormatType.transaction.rawValue

        guard let date = dateFormatterGet.date(from: string) else { return nil }
        return dateFormatterPrint.string(from: date)
    }

    static func getTransactionGroupDateString(from date: Date) -> String? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.locale = Locale(identifier: "en_US_POSIX")
        dateFormatterGet.dateFormat = DateFormatType.general.rawValue

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.locale = Locale(identifier: "en_US_POSIX")
        dateFormatterPrint.dateFormat = DateFormatType.dailyGroupedTransactions.rawValue

        return dateFormatterPrint.string(from: date)
    }

    static func isTodayDate(with date: Date) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatType.transaction.rawValue

        let todayDateString = dateFormatter.string(from: Date())
        let dateString = dateFormatter.string(from: date)

        return dateString == todayDateString
    }

    static func isYesterdayDate(with date: Date) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatType.transaction.rawValue

        let yesterdayDateString = dateFormatter.string(from: Date.yesterday)
        let dateString = dateFormatter.string(from: date)

        return dateString == yesterdayDateString
    }
}
