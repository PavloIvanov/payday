//
//  TransactionsProtocols.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit
import PayDayNetworking

protocol TransactionsConfiguratorProtocol: class {
    func configure(with viewController: TransactionsViewProtocol)
}

protocol TransactionsPresenterProtocol: class {
    func goToLogin()
    func getTransactions()
    func goToMonthlyDashboard()
}

protocol TransactionsWireframeProtocol: class {
    func routeToLogin()
    func routeToMonthlyDashboard(from viewController: UIViewController)
    func showAlertController(_ alertController: UIAlertController, from viewController: UIViewController)
}

protocol TransactionsInteractorProtocol: class {
    func logout()
    func getTransactions(completion: @escaping (Result<[DailyGroupedTransactions]>) -> Void)
}

protocol TransactionsViewProtocol: UIViewController {
    var presenter: TransactionsPresenterProtocol? { get set }

    func updateTransactions(_ transactions: [DailyGroupedTransactions])
}
