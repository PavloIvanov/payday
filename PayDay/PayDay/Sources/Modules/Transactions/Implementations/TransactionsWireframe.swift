//
//  TransactionsWireframe.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

final class TransactionsWireframe: TransactionsWireframeProtocol {

    func routeToLogin() {
        resolveService(RoutingServiceProtocol.self)?.changeRootViewController(with: LoginViewController())
    }

    func routeToMonthlyDashboard(from viewController: UIViewController) {
        let monthlyDashboardViewController = MonthlyDashboardViewController()
        viewController.navigationController?.pushViewController(monthlyDashboardViewController, animated: true)
    }

    func showAlertController(_ alertController: UIAlertController, from viewController: UIViewController) {
        viewController.present(alertController, animated: true, completion: nil)
    }
}
