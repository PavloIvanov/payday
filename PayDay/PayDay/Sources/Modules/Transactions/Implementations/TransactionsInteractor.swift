//
//  TransactionsInteractor.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import PayDayNetworking
import PayDayStorage

struct TransactionsRequest {
    static let transactions = "transactions"
}

final class TransactionsInteractor: TransactionsInteractorProtocol {

    func logout() {
        resolveService(UserServiceProtocol.self)?.clean()
        resolveService(TransactionsStorageProtocol.self)?.clean()
    }

    func getTransactions(completion: @escaping (Result<[DailyGroupedTransactions]>) -> Void) {
        if let transactions: [TransactionModel] = resolveService(TransactionsStorageProtocol.self)?.get(request: TransactionsRequest.transactions),
            !transactions.isEmpty {
            let groupedTransactions = self.groupedTransactions(from: transactions)
            DispatchQueue.main.async {
                completion(.success(groupedTransactions))
            }
            return
        }

        guard let accId = resolveService(UserServiceProtocol.self)?.getValue(for: UserServiceKeys.userId) as? Int else {
            completion(.failure("Something went wrong. Please try again later."))
            return
        }

        resolveService(TransactionsServiceProtocol.self)?.getAllTransactions(accountId: accId,
                                                                                         responseHandler: nil,
                                                                                         completion: { [weak self] (result: Result<[TransactionModel]>) in
            switch result {
            case .success(value: let transactions):
                guard let self = self else { return }
                resolveService(TransactionsStorageProtocol.self)?.put(request: TransactionsRequest.transactions,
                                                                                  with: transactions)
                let groupedTransactions = self.groupedTransactions(from: transactions)

                DispatchQueue.main.async {
                    completion(.success(groupedTransactions))
                }

            case .failure(let errorMessage):
                DispatchQueue.main.async {
                    completion(.failure(errorMessage))
                }
            }
        })
    }

    private func groupedTransactions(from transactions: [TransactionModel]) -> [DailyGroupedTransactions] {
        var groupedTransactions = [DailyGroupedTransactions]()
        guard !transactions.isEmpty else { return groupedTransactions }

        let groupedDicrionary = Dictionary(grouping: transactions) { (transaction) -> Date? in
            guard let transactionDate = DateFormatterHelper.stringToDate(string: transaction.date, format: .general) else { return nil }
            var date = Calendar.current.dateComponents([.day, .year, .month, .hour, .minute, .second], from: transactionDate)
            date.calendar = Calendar.current
            return date.date
        }

        groupedDicrionary.forEach { (date, transactions) in
            guard let groupDate = date else { return }
            let groupedModel = DailyGroupedTransactions(date: groupDate,
                                                        transactions: transactions.sorted(by: { $0.date > $1.date }))
            groupedTransactions.append(groupedModel)
        }

        return groupedTransactions.sorted(by: { $0.date > $1.date })
    }

}
