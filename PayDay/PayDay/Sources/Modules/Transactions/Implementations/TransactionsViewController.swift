//
//  TransactionsViewController.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit
import PayDayNetworking

final class TransactionsViewController: UIViewController, TransactionsViewProtocol {

    // MARK: - Public Properties
    var presenter: TransactionsPresenterProtocol?

    // MARK: - Private Properties
    private let configurator = TransactionsConfigurator()
    private var groupedTransactions: [DailyGroupedTransactions]? {
        didSet { tableView.reloadData() }
    }

    private let tableView = UITableView()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        setupUI()
    }

    // MARK: - Private Methods
    private func setupUI() {
        setupNavigationBar()
        setupTableView()
    }

    private func setupNavigationBar() {
        navigationItem.title = "Transactions"

        let logoutButton = UIBarButtonItem(title: "Logout",
                                           style: .plain,
                                           target: self,
                                           action: #selector(logoutButttonDidTapped))
        navigationItem.leftBarButtonItem = logoutButton

        let dashboardButton = UIBarButtonItem(title: "Dashboard",
                                           style: .plain,
                                           target: self,
                                           action: #selector(dashboardButttonDidTapped))
        navigationItem.rightBarButtonItem = dashboardButton
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        setupConstraintsForTableView()
        registerTableViewCells()
    }

    private func setupConstraintsForTableView() {
        view.addSubview(tableView)
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false

        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    private func registerTableViewCells() {
        tableView.register(UINib(nibName: "TransactionTableViewCell", bundle: Bundle.main),
                           forCellReuseIdentifier: String(describing: TransactionTableViewCell.self))
    }

    // NARK: - Actions
    @objc private func logoutButttonDidTapped() {
        presenter?.goToLogin()
    }

    @objc private func dashboardButttonDidTapped() {
        presenter?.goToMonthlyDashboard()
    }

    // MARK: - TransactionsViewProtocol
    func updateTransactions(_ transactions: [DailyGroupedTransactions]) {
        self.groupedTransactions = transactions
    }
}

extension TransactionsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
}

extension TransactionsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let headerDate = groupedTransactions?[section].date else { return nil }

        if DateFormatterHelper.isTodayDate(with: headerDate) {
            return "Today"
        }

        if DateFormatterHelper.isYesterdayDate(with: headerDate) {
            return "Yesterday"
        }

        if let date = groupedTransactions?[section].date {
            return DateFormatterHelper.getTransactionGroupDateString(from: date)
        }

        return ""
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return groupedTransactions?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupedTransactions?[section].transactions.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TransactionTableViewCell.self),
                                                 for: indexPath) as! TransactionTableViewCell // swiftlint:disable:this force_cast

        guard let transaction = groupedTransactions?[indexPath.section].transactions[indexPath.row] else {
            return UITableViewCell()
        }

        cell.setup(with: transaction)
        return cell
    }
}
