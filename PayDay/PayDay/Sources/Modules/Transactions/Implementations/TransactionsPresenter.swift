//
//  TransactionsPresenter.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

final class TransactionsPresenter: TransactionsPresenterProtocol {

    // MARK: - Public Properties
    weak var view: TransactionsViewProtocol?
    var interactor: TransactionsInteractorProtocol?
    var wireframe: TransactionsWireframeProtocol?

    // MARK: - Lifecycle
    init(view: TransactionsViewProtocol) {
        self.view = view
    }

    // MARK: - TransactionsPresenterProtocol
    func goToLogin() {
        interactor?.logout()
        wireframe?.routeToLogin()
    }

    func goToMonthlyDashboard() {
        guard let view = view else { return }
        wireframe?.routeToMonthlyDashboard(from: view)
    }

    func getTransactions() {
        interactor?.getTransactions(completion: { [weak self] (result) in
            switch result {
            case .success(value: let groupedTransactions):
                self?.view?.updateTransactions(groupedTransactions)

            case .failure(let errorMessage):
                self?.showAlertController(with: errorMessage)
            }
        })
    }

    // MARK: - Pirvate Methods
    private func showAlertController(with message: String) {
        let alertVC = resolveService(AlertViewHelperProtocol.self)?.createOKAlertController(title: "Error",
                                                                                            message: message)
        guard let alertViewController = alertVC,
            let view = self.view else { return }

        wireframe?.showAlertController(alertViewController, from: view)
    }
}
