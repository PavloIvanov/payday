//
//  TransactionsConfigurator.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

final class TransactionsConfigurator: TransactionsConfiguratorProtocol {
    func configure(with viewController: TransactionsViewProtocol) {
        let presenter = TransactionsPresenter(view: viewController)
        let interactor = TransactionsInteractor()
        let wireframe = TransactionsWireframe()

        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        presenter.getTransactions()
    }
}
