//
//  RegistrationInteractor.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import PayDayNetworking

final class RegistrationInteractor: RegistrationInteractorProtocol {
    func registerUser(with model: RegistrationFormModel, completion: @escaping (Result<Int>) -> Void) {
        guard isAllRequiredFieldsFullFilled(in: model) else {
            completion(.failure("Please fullfill all the required fields"))
            return
        }

        guard isPasswordsEqual(in: model) else {
            completion(.failure("Passwords are not equal"))
            return
        }

        resolveService(UserNetworkServiceProtocol.self)?.registerUser(with: model,
                                                                      email: model.email,
                                                                      phone: model.phoneNumber,
                                                                      responseHandler: nil,
                                                                      completion: { (result: Result<Int>) in
            DispatchQueue.main.async {
                if case .success(let userId) = result {
                    resolveService(UserServiceProtocol.self)?.set(value: userId, for: UserServiceKeys.userId)
                }
                completion(result)
            }
        })
    }

    func getGenders() -> [String] {
        return Gender.allCases.map { return $0.rawValue }
    }

    // MARK: - Private Methods
    private func isPasswordsEqual(in model: RegistrationFormModel) -> Bool {
        return model.password == model.confirmPassword
    }

    private func isAllRequiredFieldsFullFilled(in model: RegistrationFormModel) -> Bool {
        guard !model.firstName.isEmpty,
            !model.lastName.isEmpty,
            !model.phoneNumber.isEmpty,
            !model.email.isEmpty,
            !model.password.isEmpty,
            let confirmPass = model.confirmPassword,
            !confirmPass.isEmpty else { return false }

        return true
    }
}
