//
//  RegistrationConfigurator.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 29.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

final class RegistrationConfigurator: RegistrationConfiguratorProtocol {
    func configure(with viewController: RegistrationViewProtocol) {
        let presenter = RegistrationPresenter(view: viewController)
        let interactor = RegistrationInteractor()
        let wireframe = RegistrationWireframe(viewController: viewController)

        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.wireframe = wireframe
    }
}
