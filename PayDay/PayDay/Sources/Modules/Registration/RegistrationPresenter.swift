//
//  RegistrationPresenter.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import PayDayNetworking

final class RegistrationPresenter: RegistrationPresenterProtocol {

    // MARK: - Private Properties
    private var registrationForm: RegistrationFormModel

    // MARK: - Public Properties
    weak var view: RegistrationViewProtocol?
    var interactor: RegistrationInteractorProtocol?
    var wireframe: RegistrationWireframeProtocol?

    // MARK: - Lifecycle Properties
    init(view: RegistrationViewProtocol) {
        self.view = view
        registrationForm = RegistrationFormModel(firstName: "",
                                                 lastName: "",
                                                 phoneNumber: "",
                                                 email: "",
                                                 password: "",
                                                 confirmPassword: "",
                                                 gender: Gender.male.rawValue,
                                                 dateOfBirth: Date())
    }

    // MARK: - RegistrationPresenterProtocol
    // swiftlint:disable:next cyclomatic_complexity
    func fieldDidChange<T>(type: RegistrationField, with value: T) {
        switch type {
        case .firstName:
            guard let value = value as? String else { return }
            registrationForm.firstName = value

        case .lastName:
            guard let value = value as? String else { return }
            registrationForm.lastName = value

        case .phone:
            guard let value = value as? String else { return }
            registrationForm.phoneNumber = value

        case .email:
            guard let value = value as? String else { return }
            registrationForm.email = value

        case .gender:
            guard let value = value as? String else { return }
            registrationForm.gender = value

        case .password:
            guard let value = value as? String else { return }
            registrationForm.password = value

        case .confirmPassword:
            guard let value = value as? String else { return }
            registrationForm.confirmPassword = value

        case .birthday:
            guard let value = value as? Date else { return }
            registrationForm.dateOfBirth = value

        case .signUp, .login: break
        }
    }

    func getGenders() -> [String]? {
        return interactor?.getGenders()
    }

    func registerUser() {
        guard validate(model: registrationForm) else { return }

        interactor?.registerUser(with: registrationForm, completion: { [weak self] (result) in
            switch result {
            case .success:
                self?.wireframe?.routeToTransactions()

            case .failure(let errorMessage):
                self?.showAlertController(with: errorMessage)

            }
        })
    }

    func login() {
        wireframe?.routeToLogin()
    }

    // MARK: - Private Methods
    private func validate(model: RegistrationFormModel) -> Bool {
        guard registrationForm.password.count >= 6 else {
            self.showAlertController(with: "Password has to have more than 6 symbols")
            return false
        }

        return true
    }

    private func showAlertController(with message: String) {
        let alertVC = resolveService(AlertViewHelperProtocol.self)?.createOKAlertController(title: "Error",
                                                                                            message: message)
        guard let alertViewController = alertVC,
            let view = self.view else { return }

        wireframe?.showAlertController(alertViewController, from: view)
    }
}
