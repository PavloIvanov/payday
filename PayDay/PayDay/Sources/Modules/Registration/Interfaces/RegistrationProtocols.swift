//
//  RegistrationProtocols.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit
import PayDayNetworking

protocol RegistrationConfiguratorProtocol: class {
    func configure(with viewController: RegistrationViewProtocol)
}

protocol RegistrationWireframeProtocol: class {
    func routeToLogin()
    func routeToTransactions()
    func showAlertController(_ alertController: UIAlertController, from viewController: UIViewController)
}

protocol RegistrationViewProtocol: UIViewController {
    var presenter: RegistrationPresenterProtocol? { get set }
}

protocol RegistrationPresenterProtocol: class {
    func fieldDidChange<T>(type: RegistrationField, with value: T)
    func getGenders() -> [String]?
    func registerUser()
    func login()
}

protocol RegistrationInteractorProtocol: class {
    func registerUser(with model: RegistrationFormModel, completion: @escaping (Result<Int>) -> Void)
    func getGenders() -> [String]
}
