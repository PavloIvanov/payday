//
//  RegistrationWireframe.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

final class RegistrationWireframe {

    weak var viewController: RegistrationViewProtocol?

    init(viewController: RegistrationViewProtocol) {
        self.viewController = viewController
    }
}

extension RegistrationWireframe: RegistrationWireframeProtocol {
    func routeToLogin() {
        resolveService(RoutingServiceProtocol.self)?.changeRootViewController(with: LoginViewController())
    }

    func routeToTransactions() {
        let transactionsViewController = TransactionsViewController()
        let navigationController = UINavigationController(rootViewController: transactionsViewController)
        resolveService(RoutingServiceProtocol.self)?.changeRootViewController(with: navigationController)
    }

    func showAlertController(_ alertController: UIAlertController, from viewController: UIViewController) {
        viewController.present(alertController, animated: true, completion: nil)
    }
}
