//
//  RegistrationViewController.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

enum RegistrationField: String, CaseIterable, RawRepresentable {
    case firstName = "First Name"
    case lastName = "Last Name"
    case phone = "Phone Number"
    case email = "Email"
    case password = "Password"
    case confirmPassword = "Confirm Password"
    case gender = "Gender"
    case birthday = "Date of Birth"
    case signUp = "Sign Up"
    case login = "Login"

    var placeholder: String? {
        switch self {
        case .firstName: return "Enter your first name"
        case .lastName: return "Enter your last name"
        case .phone: return "Enter your phone number"
        case .email: return "Enter your email"
        case .password: return "Create password"
        case .confirmPassword: return "Confirm your password"
        default: return nil
        }
    }
}

final class RegistrationViewController: UIViewController {

    // MARK: - Public Properties
    var presenter: RegistrationPresenterProtocol?

    // MARK: - Private Properties
    private let configurator: RegistrationConfiguratorProtocol = RegistrationConfigurator()

    private let fields = RegistrationField.allCases
    private var genders: [String] = [String]()

    private let tableView = UITableView()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        genders = presenter?.getGenders() ?? [String]()
        setupTableView()
        registerTableViewCells()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Private Methods
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(tableView)

        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }

    private func registerTableViewCells() {
        tableView.register(UINib(nibName: "TitledTableViewCell", bundle: Bundle.main),
                           forCellReuseIdentifier: String(describing: TitledTableViewCell.self))
        tableView.register(UINib(nibName: "TitledSegmentedControlTableViewCell", bundle: Bundle.main),
                           forCellReuseIdentifier: String(describing: TitledSegmentedControlTableViewCell.self))
        tableView.register(UINib(nibName: "DayOfBirthTableViewCell", bundle: Bundle.main),
                           forCellReuseIdentifier: String(describing: DayOfBirthTableViewCell.self))
        tableView.register(UINib(nibName: "ButtonTableViewCell", bundle: Bundle.main),
                           forCellReuseIdentifier: String(describing: ButtonTableViewCell.self))
    }
}

// MARK: - RegistrationViewProtocol
extension RegistrationViewController: RegistrationViewProtocol {
    func showError(message: String) {
        // TODO: Implement validation errors in corresponding field
    }
}

// MARK: - UITableViewDelegate
extension RegistrationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 80.0

        if fields[indexPath.row] == .login || fields[indexPath.row] == .signUp {
            height = 60.0
        }

        return height
    }
}

// MARK: - UITableViewDataSource
extension RegistrationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fields.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let field = fields[indexPath.row]

        switch field {
        case .firstName, .lastName, .phone, .email, .password, .confirmPassword:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TitledTableViewCell.self),
                                                     for: indexPath) as! TitledTableViewCell // swiftlint:disable:this force_cast
            cell.delegate = self
            cell.setup(title: field.rawValue, placeholder: field.placeholder)
            return cell
        case .gender:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TitledSegmentedControlTableViewCell.self),
                                                     for: indexPath) as! TitledSegmentedControlTableViewCell // swiftlint:disable:this force_cast
            cell.delegate = self
            cell.setup(items: genders)
            return cell
        case .birthday:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DayOfBirthTableViewCell.self),
                                                     for: indexPath) as! DayOfBirthTableViewCell // swiftlint:disable:this force_cast
            cell.delegate = self
            cell.setup(title: field.rawValue)
            return cell
        case .signUp, .login:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ButtonTableViewCell.self),
                                                     for: indexPath) as! ButtonTableViewCell // swiftlint:disable:this force_cast
            cell.delegate = self
            cell.setup(buttonTitle: field.rawValue,
                       buttonBackgroundColor: .white,
                       buttonTextColor: .black)
            return cell
        }
    }
}

// MARK: - TitledTableViewCellDelegate
extension RegistrationViewController: TitledTableViewCellDelegate {
    func textFieldValueChanged(_ textField: UITextField, in cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell),
            let text = textField.text,
            !text.isEmpty else { return }

        let field = fields[indexPath.row]
        presenter?.fieldDidChange(type: field, with: text)
    }
}

// MARK: - TitledSegmentedControlTableViewCellDelegate
extension RegistrationViewController: TitledSegmentedControlTableViewCellDelegate {
    func segmentedControlValueChanged(_ sender: UISegmentedControl, in cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }

        let field = fields[indexPath.row]
        let gender = genders[sender.selectedSegmentIndex]
        presenter?.fieldDidChange(type: field, with: gender)
    }
}

// MARK: - DayOfBirthTableViewCellDelegate
extension RegistrationViewController: DayOfBirthTableViewCellDelegate {
    func valueDidChange(_ date: Date, in cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }

        let field = fields[indexPath.row]
        presenter?.fieldDidChange(type: field, with: date)
    }
}

// MARK: - ButtonTableViewCellDelegate
extension RegistrationViewController: ButtonTableViewCellDelegate {
    func buttonDidPressed(_ button: UIButton, in cell: UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }

        switch fields[indexPath.row] {
        case .login:
            presenter?.login()
        case .signUp:
            presenter?.registerUser()
        default: break
        }
    }
}
