//
//  LoginInteractor.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 29.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import PayDayNetworking

final class LoginInteractor: LoginInteractorProtocol {
    func login(email: String, password: String, completion: @escaping (Result<UserModel>) -> Void) {
        guard let result = resolveService(ValidationServiceProtocol.self)?.isValidPassword(password),
            case .success = result else {
            completion(.failure("Password is invalid"))
                return
        }

        resolveService(UserNetworkServiceProtocol.self)?.login(email: email,
                                                                           password: password,
                                                                           responseHandler: nil,
                                                                           completion: { (result: Result<UserModel>) in
            DispatchQueue.main.async {
                if case .success(let userModel) = result {
                    resolveService(UserServiceProtocol.self)?.saveUserInfo(userModel)
                }
                completion(result)
            }
        })
    }
}
