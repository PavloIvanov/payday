//
//  LoginViewController.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 29.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

final class LoginViewController: UIViewController, LoginViewProtocol {

    // MARK: - Public Properties
    var presenter: LoginPresenterProtocol?

    // MARK: - Private Properties
    private let configurator = LoginConfigurator()

    private let contentView = UIView()

    private let titleLabel = UILabel()

    private let emailLabel = UILabel()
    private let emailTextField = UITextField()

    private let passwordLabel = UILabel()
    private let passwordTextField = UITextField()

    private let loginButton = UIButton()
    private let registerButton = UIButton()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configurator.configure(with: self)
    }

    // MARK: - Private Methods
    private func setupUI() { // swiftlint:disable:this function_body_length
        // Content View
        contentView.backgroundColor = .white
        contentView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(contentView)

        contentView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        // Title Label
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                          .font: UIFont.boldSystemFont(ofSize: 24)]
        titleLabel.attributedText = NSAttributedString(string: "Welcome Back", attributes: attributes)
        titleLabel.textAlignment = .center
        contentView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 120).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true

        // Email Label
        emailLabel.text = "Email"
        view.addSubview(emailLabel)
        emailLabel.translatesAutoresizingMaskIntoConstraints = false

        emailLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30).isActive = true
        emailLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor).isActive = true
        emailLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor).isActive = true

        // Email Text Field
        emailTextField.placeholder = "Enter your email"
        emailTextField.text = "Nadiah.Spoel@example.com"
        view.addSubview(emailTextField)
        emailTextField.translatesAutoresizingMaskIntoConstraints = false

        emailTextField.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: 8).isActive = true
        emailTextField.leadingAnchor.constraint(equalTo: emailLabel.leadingAnchor).isActive = true
        emailTextField.trailingAnchor.constraint(equalTo: emailLabel.trailingAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 60.0).isActive = true

        // Passord Label
        passwordLabel.text = "Password"
        view.addSubview(passwordLabel)
        passwordLabel.translatesAutoresizingMaskIntoConstraints = false

        passwordLabel.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 20).isActive = true
        passwordLabel.leadingAnchor.constraint(equalTo: emailTextField.leadingAnchor).isActive = true
        passwordLabel.trailingAnchor.constraint(equalTo: emailTextField.trailingAnchor).isActive = true

        // Password Text Field
        passwordTextField.placeholder = "Enter your password"
        passwordTextField.isSecureTextEntry = true
        passwordTextField.text = "springs"
        view.addSubview(passwordTextField)
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false

        passwordTextField.topAnchor.constraint(equalTo: passwordLabel.bottomAnchor, constant: 8).isActive = true
        passwordTextField.leadingAnchor.constraint(equalTo: passwordLabel.leadingAnchor).isActive = true
        passwordTextField.trailingAnchor.constraint(equalTo: passwordLabel.trailingAnchor).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 60.0).isActive = true

        // Login Button
        loginButton.setTitle("Login", for: .normal)
        loginButton.setTitleColor(.black, for: .normal)
        loginButton.addTarget(self, action: #selector(loginButtonDidTapped), for: .touchUpInside)
        view.addSubview(loginButton)
        loginButton.translatesAutoresizingMaskIntoConstraints = false

        loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 8).isActive = true
        loginButton.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor).isActive = true
        loginButton.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 60.0).isActive = true

        // Register Button
        registerButton.setTitle("Register", for: .normal)
        registerButton.setTitleColor(.black, for: .normal)
        registerButton.addTarget(self, action: #selector(registerButtonDidTapped), for: .touchUpInside)
        view.addSubview(registerButton)
        registerButton.translatesAutoresizingMaskIntoConstraints = false

        registerButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        registerButton.leadingAnchor.constraint(equalTo: passwordLabel.leadingAnchor).isActive = true
        registerButton.trailingAnchor.constraint(equalTo: passwordLabel.trailingAnchor).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
    }

    // MARK: - Actions
    @objc private func loginButtonDidTapped() {
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""

        presenter?.login(email: email, password: password)
    }

    @objc private func registerButtonDidTapped() {
        presenter?.routeToRegisteration()
    }
}
