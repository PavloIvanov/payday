//
//  LoginWireframe.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 29.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

final class LoginWireframe: LoginWireframeProtocol {

    func routeToRegisteration() {
        resolveService(RoutingServiceProtocol.self)?.changeRootViewController(with: RegistrationViewController())
    }

    func routeToMenu() {
        let transactionsViewController = TransactionsViewController()
        let navigationController = UINavigationController(rootViewController: transactionsViewController)
        resolveService(RoutingServiceProtocol.self)?.changeRootViewController(with: navigationController)
    }

    func showAlertController(_ alertController: UIAlertController, from viewController: UIViewController) {
        viewController.present(alertController, animated: true, completion: nil)
    }
}
