//
//  LoginConfigurator.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 29.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

final class LoginConfigurator: LoginConfiguratorProtocol {
    func configure(with viewController: LoginViewProtocol) {
        let presenter = LoginPresenter(view: viewController)
        let interactor = LoginInteractor()
        let wireframe = LoginWireframe()

        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.wireframe = wireframe
    }
}
