//
//  LoginPresenter.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 29.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

final class LoginPresenter: LoginPresenterProtocol {

    // MARK: - Public Properties
    weak var view: LoginViewProtocol?
    var interactor: LoginInteractorProtocol?
    var wireframe: LoginWireframeProtocol?

    // MARK: - Lifecycle
    init(view: LoginViewProtocol) {
        self.view = view
    }

    // MARK: - LoginPresenterProtocol
    func login(email: String, password: String) {
        guard !email.isEmpty else {
            showAlertController(with: "Please, fullfill email fieldd.")
            return
        }

        guard !password.isEmpty else {
            showAlertController(with: "Please, fullfill password field.")
            return
        }

        interactor?.login(email: email, password: password, completion: { [weak self] (result) in
            switch result {
            case .success:
                self?.wireframe?.routeToMenu()

            case .failure(let message):
                self?.showAlertController(with: message)
            }
        })
    }

    func routeToRegisteration() {
        wireframe?.routeToRegisteration()
    }

    // MARK: - Private Methods
    private func showAlertController(with message: String) {
        let alertVC = resolveService(AlertViewHelperProtocol.self)?.createOKAlertController(title: "Error",
                                                                                            message: message)
        guard let alertViewController = alertVC,
            let view = self.view else { return }

        wireframe?.showAlertController(alertViewController, from: view)
    }
}
