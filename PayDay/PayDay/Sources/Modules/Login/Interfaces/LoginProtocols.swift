//
//  LoginProtocols.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 29.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit
import PayDayNetworking

protocol LoginConfiguratorProtocol: class {
    func configure(with viewController: LoginViewProtocol)
}

protocol LoginWireframeProtocol: class {
    func routeToRegisteration()
    func routeToMenu()
    func showAlertController(_ alertController: UIAlertController, from viewController: UIViewController)
}

protocol LoginPresenterProtocol: class {
    func login(email: String, password: String)
    func routeToRegisteration()
}

protocol LoginInteractorProtocol: class {
    func login(email: String, password: String, completion: @escaping (Result<UserModel>) -> Void)
}

protocol LoginViewProtocol: UIViewController {
    var presenter: LoginPresenterProtocol? { get set }
}
