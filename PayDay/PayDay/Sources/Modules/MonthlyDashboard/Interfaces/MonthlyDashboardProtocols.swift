//
//  MontlyDashboardProtocols.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit
import PayDayNetworking

protocol MonthlyDashboardConfiguratorProtocol: class {
    func configure(with viewController: MonthlyDashboardViewProtocol)
}

protocol MonthlyDashboardPresenterProtocol: class {
    func getReports()
}

protocol MonthlyDashboardWireframeProtocol: class {
    func showAlertController(_ alertController: UIAlertController, from viewController: UIViewController)
}

protocol MonthlyDashboardInteractorProtocol: class {
    func getReports(completion: @escaping (Result<[MonthlyExpensesModel]>) -> Void)
}

protocol MonthlyDashboardViewProtocol: UIViewController {
    var presenter: MonthlyDashboardPresenterProtocol? { get set }

    func updateDashboard(expenses: [MonthlyExpensesModel])
}
