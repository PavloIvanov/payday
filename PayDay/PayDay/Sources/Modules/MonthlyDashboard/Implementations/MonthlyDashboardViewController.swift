//
//  MonthlyDashboardViewController.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

class MonthlyDashboardViewController: UIViewController, MonthlyDashboardViewProtocol {

    // MARK: - Public Properties
    var presenter: MonthlyDashboardPresenterProtocol?

    // MARK: - Private Properties
    private let configurator = MonthlyDashboardConfigurator()
    private let tableView = UITableView()
    private var expenses: [MonthlyExpensesModel]? {
        didSet { tableView.reloadData() }
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        setupUI()
    }

    // MARK: - Private Methods
    private func setupUI() {
        navigationItem.title = "Monthly Dashboard"
        setupTableView()
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self

        tableView.separatorStyle = .none
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false

        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        registerTableViewCells()
    }

    private func registerTableViewCells() {
        tableView.register(UINib(nibName: "DashboardCategoryTableViewCell", bundle: Bundle.main),
                           forCellReuseIdentifier: String(describing: DashboardCategoryTableViewCell.self))
    }

    // MARK: - MonthlyDashboardViewProtocol
    func updateDashboard(expenses: [MonthlyExpensesModel]) {
        self.expenses = expenses
    }
}

// MARK: - UITableViewDelegate
extension MonthlyDashboardViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80.0
    }
}

// MARK: - UITableViewDataSource
extension MonthlyDashboardViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let monthlyExpenses = expenses?[section] else { return nil }
        let view = MonthlyExpenseHeaderView()
        view.setup(date: monthlyExpenses.date,
                   amount: monthlyExpenses.sum)

        return view
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return expenses?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenses?[section].expenses.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let expense = expenses?[indexPath.section].expenses[indexPath.row] else {
            return UITableViewCell()
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DashboardCategoryTableViewCell.self),
                                                 for: indexPath) as! DashboardCategoryTableViewCell // swiftlint:disable:this force_cast
        cell.setup(with: expense)

        return cell
    }
}
