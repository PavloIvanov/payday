//
//  MonthlyDashboardPresenter.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

class MonthlyDashboardPresenter: MonthlyDashboardPresenterProtocol {

    // MARK: - Public Properties
    weak var view: MonthlyDashboardViewProtocol?
    var interactor: MonthlyDashboardInteractorProtocol?
    var wireframe: MonthlyDashboardWireframeProtocol?

    // MARK: - Lifecycle
    init(view: MonthlyDashboardViewProtocol) {
        self.view = view
    }

    // MARK: - MonthlyDashboardPresenterProtocol
    func getReports() {
        interactor?.getReports(completion: { [weak self] (result) in
            switch result {
            case .success(value: let expenses):
                self?.view?.updateDashboard(expenses: expenses)

            case .failure(let errorMessage):
                self?.showAlertController(with: errorMessage)
            }
        })
    }

    // MARK: - Pirvate Methods
    private func showAlertController(with message: String) {
        let alertVC = resolveService(AlertViewHelperProtocol.self)?.createOKAlertController(title: "Error",
                                                                                            message: message)
        guard let alertViewController = alertVC,
            let view = self.view else { return }

        wireframe?.showAlertController(alertViewController, from: view)
    }
}
