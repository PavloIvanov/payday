//
//  MonthlyDashboardConfigurator.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

class MonthlyDashboardConfigurator: MonthlyDashboardConfiguratorProtocol {

    func configure(with viewController: MonthlyDashboardViewProtocol) {
        let presenter = MonthlyDashboardPresenter(view: viewController)
        let interactor = MonthlyDashboardInteractor()
        let wireframe = MonthlyDashboardWireframe()

        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        presenter.getReports()
    }
}
