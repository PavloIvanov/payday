//
//  MonthlyDashboardInteractor.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation
import PayDayNetworking

class MonthlyDashboardInteractor: MonthlyDashboardInteractorProtocol {

    func getReports(completion: @escaping (Result<[MonthlyExpensesModel]>) -> Void) {

        guard let accId = resolveService(UserServiceProtocol.self)?.getValue(for: UserServiceKeys.userId) as? Int else {
            completion(.failure("Something went wrong. Please try again later."))
            return
        }

        resolveService(TransactionsServiceProtocol.self)?.getAllTransactions(accountId: accId,
                                                                             responseHandler: nil,
                                                                             completion: { [weak self] (result: Result<[TransactionModel]>) in
            switch result {
            case .success(value: let transactions):
                guard let self = self else { return }

                DispatchQueue.main.async {
                    completion(.success(self.groupTransactions(transactions)))
                }

            case .failure(let errorMessage):
                DispatchQueue.main.async {
                    completion(.failure(errorMessage))
                }
            }
        })
    }

    private func groupTransactions(_ transactions: [TransactionModel]) -> [MonthlyExpensesModel] {
        let groupedTransactionsByMonth = Dictionary(grouping: transactions) { (transaction) -> Date? in
            guard let transactionDate = DateFormatterHelper.stringToDate(string: transaction.date, format: .general) else { return nil }
            var date = Calendar.current.dateComponents([.year, .month], from: transactionDate)
            date.calendar = Calendar.current
            return date.date
        }

        var resultArray = [MonthlyExpensesModel]()
        for (date, transactions) in groupedTransactionsByMonth {
            var totalAmountOfMonth = 0.0

            let groupedTransactionsByCategory = Dictionary(grouping: transactions) { (transaction) -> String in
                totalAmountOfMonth += Double(transaction.amount) ?? 0.0
                return transaction.category
            }

            var categories = [CategoryExpensesModel]()
            groupedTransactionsByCategory.forEach {
                let transaction = Array($0.value)
                let totalAmountOfCategory: Double = transaction.reduce(into: 0) { $0 += Double($1.amount) ?? 0.0 }
                let categoryExpensesModel = CategoryExpensesModel(categoryName: $0.key,
                                                                  amount: totalAmountOfCategory)
                categories.append(categoryExpensesModel)
            }

            if let monthDate = date {
                resultArray.append(MonthlyExpensesModel(date: monthDate,
                                                        sum: totalAmountOfMonth,
                                                        expenses: categories))
            }
        }

        return resultArray.sorted(by: { $0.date > $1.date })
    }
}
