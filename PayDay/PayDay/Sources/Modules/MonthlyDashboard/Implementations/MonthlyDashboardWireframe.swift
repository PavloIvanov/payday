//
//  MonthlyDashboardWireframe.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

class MonthlyDashboardWireframe: MonthlyDashboardWireframeProtocol {

    func showAlertController(_ alertController: UIAlertController, from viewController: UIViewController) {
        viewController.present(alertController, animated: true, completion: nil)
    }
}
