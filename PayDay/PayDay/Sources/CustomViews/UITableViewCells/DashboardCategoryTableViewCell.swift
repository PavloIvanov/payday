//
//  DashboardCategoryTableViewCell.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

class DashboardCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var coloredView: UIView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        coloredView.layer.cornerRadius = 5.0
    }

    func setup(with model: CategoryExpensesModel) {
        categoryNameLabel.text = model.categoryName
        amountLabel.text = String(format: "%.2f $", model.amount)
    }
}
