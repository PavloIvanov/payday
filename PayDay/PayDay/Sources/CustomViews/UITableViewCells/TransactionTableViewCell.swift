//
//  TransactionTableViewCell.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit
import PayDayNetworking

class TransactionTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var coloredView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()

        coloredView.layer.cornerRadius = 5.0
    }

    // MARK: - Public Methods
    func setup(with model: TransactionModel) {
        dateLabel.text = DateFormatterHelper.getTransactionDateString(from: model.date)
        descriptionLabel.text = model.vendor
        amountLabel.text = "\(model.amount)$"
    }
}
