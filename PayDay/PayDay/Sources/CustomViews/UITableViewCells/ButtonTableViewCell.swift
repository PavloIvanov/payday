//
//  ButtonTableViewCell.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

protocol ButtonTableViewCellDelegate: class {
    func buttonDidPressed(_ button: UIButton, in cell: UITableViewCell)
}

class ButtonTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var button: UIButton!

    // MARK: - Public Properties
    weak var delegate: ButtonTableViewCellDelegate?

    // MARK: - Public Methods
    func setup(buttonTitle: String, buttonBackgroundColor: UIColor, buttonTextColor: UIColor) {
        button.setTitle(buttonTitle, for: .normal)
        button.setTitle(buttonTitle, for: .selected)
        button.backgroundColor = buttonBackgroundColor
        button.titleLabel?.textColor = buttonTextColor
    }

    // MARK: - Actions
    @IBAction func buttonDidPressed(_ sender: Any) {
        guard let button = sender as? UIButton else { return }
        delegate?.buttonDidPressed(button, in: self)
    }
}
