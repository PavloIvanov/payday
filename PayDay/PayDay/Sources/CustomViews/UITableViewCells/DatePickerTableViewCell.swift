//
//  DayOfBirthTableViewCell.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

protocol DayOfBirthTableViewCellDelegate: class {
    func valueDidChange(_ date: Date, in cell: UITableViewCell)
}

class DayOfBirthTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var dayOfBirthTextField: UITextField!
    @IBOutlet weak var monthOfBirthTextField: UITextField!
    @IBOutlet weak var yearOfBirthTextField: UITextField!

    // MARK: - Public Properties
    weak var delegate: DayOfBirthTableViewCellDelegate?

    // MARK: - Private Properties
    private var dateComponents: DateComponents?

    // MARK: - Public Methods
    func setup(title: String) {
        titleLabel.text = title
    }

    // MARK: - Private Methods
    private func dateDidChanged() {
        guard let components = dateComponents,
            let date = Calendar.current.date(from: components) else { return }

        delegate?.valueDidChange(date, in: self)
    }

    // MARK: - Actions
    @IBAction func dayTextFieldDidChaged(_ sender: Any) {
        guard let textField = sender as? UITextField,
            let text = textField.text,
            !text.isEmpty else { return }
        dateComponents?.day = Int(text)
        dateDidChanged()
    }

    @IBAction func monthTextFieldDidChanged(_ sender: Any) {
        guard let textField = sender as? UITextField,
            let text = textField.text,
            !text.isEmpty else { return }
        dateComponents?.month = Int(text)
        dateDidChanged()
    }

    @IBAction func yearTextFieldDidChanged(_ sender: Any) {
        guard let textField = sender as? UITextField,
            let text = textField.text,
            !text.isEmpty else { return }
        dateComponents?.year = Int(text)
        dateDidChanged()
    }
}
