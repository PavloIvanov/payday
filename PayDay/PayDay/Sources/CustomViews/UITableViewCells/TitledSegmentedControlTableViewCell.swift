//
//  TitledSegmentedControlTableViewCell.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

protocol TitledSegmentedControlTableViewCellDelegate: class {
    func segmentedControlValueChanged(_ sender: UISegmentedControl, in cell: UITableViewCell)
}

class TitledSegmentedControlTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    // MARK: - Public Properties
    var indexPath: IndexPath?
    weak var delegate: TitledSegmentedControlTableViewCellDelegate?

    // MARK: - Public Properties
    func setup(items: [String]) {
        guard !items.isEmpty else { return }

        segmentedControl.removeAllSegments()

        for (index, item) in items.enumerated() {
            segmentedControl.insertSegment(withTitle: item, at: index, animated: false)
        }

        segmentedControl.selectedSegmentIndex = 0
    }

    // MARK: - Actions
    @IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        delegate?.segmentedControlValueChanged(sender, in: self)
    }
}
