//
//  TitledTableViewCell.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 28.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

protocol TitledTableViewCellDelegate: class {
    func textFieldValueChanged(_ textField: UITextField, in cell: UITableViewCell)
}

class TitledTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var textField: UITextField!

    // MARK: - Public Properties
    var indexPath: IndexPath?
    weak var delegate: TitledTableViewCellDelegate?

    // MARK: - Public Methods
    func setup(title: String, placeholder: String?) {
        self.title.text = title
        textField.placeholder = placeholder
    }

    // MARK: - Actions
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        delegate?.textFieldValueChanged(sender, in: self)
    }
}
