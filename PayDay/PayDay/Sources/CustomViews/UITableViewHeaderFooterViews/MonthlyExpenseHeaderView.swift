//
//  MonthlyExpenseHeaderView.swift
//  PayDay
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import UIKit

class MonthlyExpenseHeaderView: UIView {

    private let dateLabel = UILabel()
    private let amountLabel = UILabel()
    private let containerView = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        containerView.backgroundColor = .white
        addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false

        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true

        containerView.addSubview(dateLabel)
        dateLabel.textColor = .gray
        dateLabel.translatesAutoresizingMaskIntoConstraints = false

        dateLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8).isActive = true
        dateLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8).isActive = true
        dateLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8).isActive = true

        containerView.addSubview(amountLabel)
        amountLabel.translatesAutoresizingMaskIntoConstraints = false

        amountLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 8).isActive = true
        amountLabel.leadingAnchor.constraint(equalTo: dateLabel.leadingAnchor).isActive = true
        amountLabel.trailingAnchor.constraint(equalTo: dateLabel.trailingAnchor).isActive = true
        amountLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8).isActive = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(date: Date, amount: Double) {
        dateLabel.text = DateFormatterHelper.stringFromDate(date: date, to: .dashboardMonthYear)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                          .font: UIFont.boldSystemFont(ofSize: 20)]
        let amountString = String(format: "%.2f", amount)
        amountLabel.attributedText = NSAttributedString(string: "$ \(amountString)", attributes: attributes)
    }
}
