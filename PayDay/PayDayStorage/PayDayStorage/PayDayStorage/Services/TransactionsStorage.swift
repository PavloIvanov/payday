//
//  TransactionsStorage.swift
//  PayDayStorage
//
//  Created by Pavlo Ivanov on 30.07.2020.
//  Copyright © 2020 Pavlo Ivanov. All rights reserved.
//

import Foundation

public typealias Request = String

public protocol TransactionsStorageProtocol: class {
    func get<T: Hashable>(request: Request) -> T?
    @discardableResult func put<T: Hashable>(request: Request, with value: T) -> Bool
    func clean()
}

public class TransactionsStorage: TransactionsStorageProtocol {

    private var storage = [String: AnyHashable]()

    public init() { }

    public func get<T: Hashable>(request: Request) -> T? {
        return storage[request] as? T
    }

    @discardableResult public func put<T: Hashable>(request: Request, with value: T) -> Bool {
        storage[request] = value
        return true
    }

    public func clean() {
        storage.removeAll()
    }
}
