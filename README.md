# PayDay

- Project Name: PayDay
- Deployemnt Target: iOS 12
- Language: Swift 5
- Architecture: VIPER
- Package Manager: CocoaPods 1.7.4
- Short Description: The app for banking purposes.

**This project consists of four modules:**
1. PayDay
2. PayDayStorage
3. PayDayNetworking
4. Pods

**Modules description.**
1. PayDay - main module. Here are UI components and the business logic.
2. PayDayNetworking - module that is used for the fetching different data or resources that are outside of the scope of the app.
3. PayDayStorage - module that is used for the fetching and the storing different data or resources that are in the scope of the app.
4. Pods - package manager.

**Third-Party Libraries:**
1. Swinject - makes it easy to implement dependency injection.
2. IQKeyboardManagerSwift - handles keyboard showing and hiding.
3. SwiftLint - static analyzer of code.

**What I haven't done due to lack of time or what I would recommend to improve:**
1. Validation service.
2. Move cells' identifiers in cells' classes.
3. Create the date picker for the birthday field on the registration screen.
4. Create custom model that will hold properties for customizing cells' UI components. (color or cornerRadius for UIButton, attributed string for title etc).
5. Create UIView with UILabel and UITextField (this will fix the amount of code that setups constraints for views in LoginViewController).
6. Create separate files and classes to organize the constants.
7. Unit and Integration tests.
8. Implement global resolve function for DIContainer instead of the calling these long expressions.
9. Create base classes with generic for wireframe, presenter and view.  F.e. "class BaseView<T: BasePresenterProtocol>" that will have "presenter" property.
10. Make UI more beautiful, apply assets.

**How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't
spend much time on the coding test then use this as an opportunity to explain what you would add.**
- I didn't have time to implement module/integration tests, but I've tested all functionality by myself. I checked all the cases for each module right after I implemented that, so in summary it took me ~1 hour. I think the most important parts of the code that have to be covered are transactions fetching, business logic of transactions/dashboard and UI that is responsible for transactions list/dashboard showing, because this functional has the biggest value.

**What was the most useful feature that was added to the latest version of your chosen language? Please include a
snippet of code that shows how you've used it.**
- SwiftUI is the most important and great thing that Apple introduced in the latest versions of Swift. Currently I don't use it in real projects.
```
NavigationView {
    List {
        Text("Hello World")
        Text("Hello World")
        Text("Hello World")
    }
    .navigationBarTitle("Menu")
}
```


**How would you track down a performance issue in production? Have you ever had to do this?**
- Yes, I have such kind of experience. My coworkers and I have used Firebase analytics to track many aspects of our app and that also includes some performance tracking.

**How would you debug issues related to API usage? Can you give us an example?**
- I always use Postman(the app for the testing of the API) and Charles(proxy) and some kind of network logger class (service) if I have some troubles, issues or bugs that are related to API usage.

**How would you improve the Node server’s API that you just used? Please describe yourself using JSON.**
1. Everywhere and always use camelcase style for names of fields (not 'account_id' or 'First Name').
"accountId", not "account_id"
"firstName", not "First Name"

2. Doesn't pass user's password in response or if there is some need for that -- use some encryption.
{ "password": "5UrSKXVG7IXIjK7k50Wy3f4vT0lRIsRyTEh8pEwDC02EyJx3CoHwTE3qNjw4" }
not { "password": "qwerty123" }

3. Email and phone checks for existing in the DB have to be done on the back-end because it's not the responsibility of front-end. We need to make front-end as lightweight as possible.

4. The process of grouping and sorting of transactions has to be done on the server side as a separate endpoint to make front-end as lightweight as possible as I've mentioned before.
```
`[
  {
  "date": "someData",
  "transactions": 
    [
      {
        "date": "someDate",
        "count": "-9.00",
        "description": "TRF/Nepa Bill/FRM Richard"
      }, 
      {
        "date": "someDate",
        "count": "-11.00",
        "description": "McDonalds"
      }
    ]
  },
  { 
  "date": "someData2",
  "transactions": 
    [
       {
        "date": "someDate",
        "count": "-27.00",
        "description": "Online store"
      },
      {
        "date": "someDate",
        "count": "-11.00",
        "description": "Oil"
      },
      {
        "date": "someDate",
        "count": "-100.00",
        "description": "John Smith"
      },
      {
        "date": "someDate",
        "count": "-4.00",
        "description": "Transport"
      }
    ]
  } 
]`
```


5. The separate endpoint is needed to allow the app fetch the dashboard data in the appropriate for the front-end format.
```
`{
    "date": "someDate",
    "sum": 3,328,
    "expenses": 
    [
    {
        "categoryName": "Food",
        "amount": -1,209.00
    },
    {
        "categoryName": "Clothes",
        "amount": -1,189.00
    }, 
    {
        "categoryName": "Cash",
        "amount": -930.00
    }
    ]
}`
```


6. All number fields have to be sended as Double, not as String.
{ "amount":  -1024.01 }
not { "amount": "-1024.01" }

7. It would be better if the API will send a user token on login/registration endpoint.
